import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TAG_INNER_APPModule } from './app/TAG_APP.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(TAG_INNER_APPModule)
  .catch(err => console.log(err));
