import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TAG_INNER_APPComponent } from './TAG_APP.component';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', component: TAG_INNER_APPComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TAG_INNER_APPRoutingModule { }
