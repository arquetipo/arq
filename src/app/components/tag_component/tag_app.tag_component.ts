import { Component, OnInit } from '@angular/core';
import { NuarArchitectureService } from 'bancosantander/src/app/core/services/nuar.architecture.service';

@Component({
  selector: 'TAG_APP-TAG_COMPONENT',
  templateUrl: './TAG_APP.TAG_COMPONENT.html',
})
export class TAG_INNER_APPTAG_INNER_COMPONENTComponent implements OnInit{
  title = 'TAG_INNER_APP.TAG_COMPONENT';

  constructor(private architectureService: NuarArchitectureService) {    
  }


  ngOnInit() {
  }  

}