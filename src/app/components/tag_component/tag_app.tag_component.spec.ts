import { TestBed, async } from '@angular/core/testing';
import { TAG_INNER_APPTAG_INNER_COMPONENTComponent } from './TAG_APP.TAG_COMPONENT';

describe('TAG_INNER_APPTAG_INNER_COMPONENTComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TAG_INNER_APPTAG_INNER_COMPONENTComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(TAG_INNER_APPTAG_INNER_COMPONENTComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
