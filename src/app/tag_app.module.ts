// Angular Core
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { TranslateModule, TranslateLoader, TranslateStaticLoader, TranslateService } from 'ng2-translate';

// Nuar Architecture
import { NuarCoreModule } from '../../node_modules/bancosantander/src/app/core/nuar.core.module';
import { GlobalValidators } from '../../node_modules/bancosantander/src/app/core/validators/globalvalidators';
import { GlobalErrorHandler } from '../../node_modules/bancosantander/src/app/nuar.globalerrorhandler';
import { ErrorService } from '../../node_modules/bancosantander/src/app/core/services/error/nuar.error.service';

// TAG_INNER_APP
import { TAG_INNER_APPComponent } from './TAG_APP.component';
import { TAG_INNER_APPRoutingModule } from './TAG_APP.routing.module';

//Architecture translate function
export function translateLoaderFactory(http: any) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    TAG_INNER_APPComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    TAG_INNER_APPRoutingModule,
    NuarCoreModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: translateLoaderFactory,
      deps: [Http]
    })
  ],
  exports: [
  ],
  providers: [
    { provide: GlobalValidators, useClass: GlobalValidators },
    // register global error handler Architecture
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    ErrorService
  ],
  bootstrap: [TAG_INNER_APPComponent]
})
export class TAG_INNER_APPModule { 

  constructor(private translate: TranslateService) {
        // Setting default language as Spanish
        translate.setDefaultLang('es');
      }

}
