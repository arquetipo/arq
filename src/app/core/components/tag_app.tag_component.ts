import { Component, OnInit } from '@angular/core';
import { ArchitectureService } from 'bancosantander/src/app/core/services/nuar.architecture.service';

@Component({
  selector: 'TAG_APP-TAG_COMPONENT',
  templateUrl: './TAG_APP.TAG_COMPONENT.html',
})
export class TAG_APP_INNERTAG_COMPONENT_INNERComponent implements OnInit{
  title = 'TAG_APP_INNER.TAG_COMPONENT';

  constructor(private architectureService: ArchitectureService) {    
  }


  ngOnInit() {
  }  

}