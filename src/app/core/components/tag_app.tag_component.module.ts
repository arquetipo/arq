import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TAG_APP_INNERTAG_COMPONENT_INNERComponent } from './TAG_APP.TAG_COMPONENT';


@NgModule({
  declarations: [
    TAG_APP_INNERTAG_COMPONENT_INNERComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
})
export class TAG_APP_INNERTAG_COMPONENT_INNERModule { }