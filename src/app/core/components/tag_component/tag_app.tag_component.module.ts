import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TAG_INNER_APPTAG_INNER_COMPONENTComponent } from './TAG_APP.TAG_COMPONENT';


@NgModule({
  declarations: [
    TAG_INNER_APPTAG_INNER_COMPONENTComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
})
export class TAG_INNER_APPTAG_INNER_COMPONENTModule { }