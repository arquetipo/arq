import { TestBed, async } from '@angular/core/testing';
import { TAG_APP_INNERTAG_COMPONENT_INNERComponent } from './TAG_APP.TAG_COMPONENT';

describe('TAG_APP_INNERTAG_COMPONENT_INNERComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TAG_APP_INNERTAG_COMPONENT_INNERComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(TAG_APP_INNERTAG_COMPONENT_INNERComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
