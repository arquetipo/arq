import { TestBed, inject } from '@angular/core/testing';
import { TAG_APP_INNERTAG_COMPONENT_INNERService } from './TAG_APP.TAG_COMPONENT.service';

describe('TAG_APP_INNERTAG_COMPONENT_INNERService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TAG_APP_INNERTAG_COMPONENT_INNERService]
    });
  });
  
  it('should be created', inject([TAG_APP_INNERTAG_COMPONENT_INNERService], (service:TAG_APP_INNERTAG_COMPONENT_INNERService) => {
    expect(service).toBeTruthy();
    })); 
  
});
