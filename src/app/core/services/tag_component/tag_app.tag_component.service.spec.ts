import { TestBed, inject } from '@angular/core/testing';
import { TAG_INNER_APPTAG_INNER_COMPONENTService } from './TAG_APP.TAG_COMPONENT.service';

describe('TAG_INNER_APPTAG_INNER_COMPONENTService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TAG_INNER_APPTAG_INNER_COMPONENTService]
    });
  });
  
  it('should be created', inject([TAG_INNER_APPTAG_INNER_COMPONENTService], (service:TAG_INNER_APPTAG_INNER_COMPONENTService) => {
    expect(service).toBeTruthy();
    })); 
  
});
